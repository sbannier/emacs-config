(setq custom-file "~/.emacs.d/custom.el")
(load custom-file 'noerror)


;; Increase the garbage collector limit during init, then reset to old value
(setq gc-cons-threshold 64000000)
(add-hook 'after-init-hook (lambda ()
                             (custom-reevaluate-setting 'gc-cons-threshold)))



(server-start)



;;;----------------------------------------
;;; Package manager settings
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ;;("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ))
(package-initialize)



;;;----------------------------------------
;;; Install use-package leaf manually if not present All other
;;; packages are managed using these packages
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))



;;;----------------------------------------
;;; Some basic settings


;; Load my theme
(load-theme 'tango-dark t)




;; Remove all useless stuff
(setq inhibit-startup-screen t)
(menu-bar-mode -1)
(tool-bar-mode -1)
(setq tool-bar-style nil)


;; Scrollbar and related stuff
(set-scroll-bar-mode 'left)
(setq scroll-margin 1)
(setq scroll-preserve-screen-position 1)
;; Explicitly disable horizontal scroll bar
(horizontal-scroll-bar-mode -1)
(setq mouse-wheel-progressive-speed nil)




(setq-default truncate-lines t)

;; Show line numbers even if line width is long (default was 200)
(setq line-number-display-limit-width 500)

;; Show column numbers
(column-number-mode t)


;; Make sentences end with single space
(setq-default sentence-end-double-space nil)

;; Stop creating those backup~ files
(setq make-backup-files nil)

;; Save/restore opened files
(desktop-save-mode 1)
;; Do not save the mouse color when saving the desktop
(push '(mouse-color . :never) frameset-filter-alist)


;; Disable bell
(setq ring-bell-function 'ignore)

;; Indent with spaces only
(setq-default indent-tabs-mode nil)



;;; Remove the annoying Ctrl-z shortcut for graphical sessions,
;;; Ctrl-x Ctrl-z remains unset, I never use that one.
(when (display-graphic-p)
  (global-unset-key (kbd "C-z")))
(global-unset-key (kbd "C-x C-z"))
;;; Remove 'save-some-buffers' from Ctrl-x s shortcut
(global-unset-key (kbd "C-x s"))
;;; Remove 'kmacro-edit-macro' from Ctrl-x Ctrl-k RETURN
(global-unset-key (kbd "C-x C-k RET"))
;;; Remove 'keyboard-escape-quit' from ESC ESC ESC
(global-unset-key (kbd "ESC ESC ESC"))



;;; Enable functions that are disabled by default
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'scroll-left 'disabled nil)



;;;----------------------------------------
;;; dired
;; Disable drag-and-drop in dired
(setq dired-dnd-protocol-alist nil)



;;;----------------------------------------
;;; C mode
(setq c-default-style
      '((c-mode . "ellemtel")
        (c++-mode . "ellemtel")
        (java-mode . "java")
        (awk-mode . "awk")
        (other . "gnu")))




;;;----------------------------------------
;;; grep config
(setq grep-highlight-matches t)



;;;----------------------------------------
;;; auto revert mode
;; Update buffer if associated file changes on disk
(global-auto-revert-mode 1)
;; Suppress warning about 'reverting buffer ...' in dired mode
(add-hook 'dired-mode-hook (lambda ()
                             (make-local-variable 'auto-revert-verbose)
                             (setq auto-revert-verbose nil)))











;;;----------------------------------------
;; For benchmarking the init sequence
;;(use-package benchmark-init
;;  :ensure t
;;  :config
;;  ;; To disable collection of benchmark data after init is done.
;;  (add-hook 'after-init-hook 'benchmark-init/deactivate))



;;;----------------------------------------
;;; guru mode
(use-package guru-mode
  :ensure t
  :demand t
  :diminish guru-mode
  :bind ("<f12>" . guru-global-mode)
  :config
  (guru-global-mode 1))



;;;----------------------------------------
;;; windresize
;;; Resize windows via keyboard
(use-package windresize
  :ensure t
  :config
  (global-set-key (kbd "C-c r") 'windresize))



;;;----------------------------------------
;;; vterm (only on non-windows machines)
(unless (eq system-type 'windows-nt)
  (use-package vterm
    :ensure t
    :config
    (set-face-attribute 'vterm-color-black   nil :foreground "#2E3436" :background "#555753")
    (set-face-attribute 'vterm-color-blue    nil :foreground "#3465A4" :background "#729FCF")
    (set-face-attribute 'vterm-color-green   nil :foreground "#4E9A06" :background "#8AE234")
    (set-face-attribute 'vterm-color-cyan    nil :foreground "#06989A" :background "#34E2E2")
    (set-face-attribute 'vterm-color-red     nil :foreground "#CC0000" :background "#EF2929")
    (set-face-attribute 'vterm-color-magenta nil :foreground "#75507B" :background "#AD7FA8")
    (set-face-attribute 'vterm-color-yellow  nil :foreground "#C4A000" :background "#FCE94F")
    (set-face-attribute 'vterm-color-white   nil :foreground "#D3D7CF" :background "#EEEEEC")))


;;; eshell
(use-package eshell
  :ensure t
  :hook (eshell-mode . (lambda ()
                         ;; Line wrapping
                         (setq truncate-lines nil)
                         ;; Company mode is annoying in eshell, deactivate it
                         (company-mode 0)
                         ;; Completion like in other shells
                         (local-set-key (kbd "<tab>") 'completion-at-point)
                         ;; Add my custom completion function
                         (add-to-list 'completion-at-point-functions 'my-eshell-complete-slash)))
  :config
  ;; Complete '..' to '../'
  (defun my-eshell-complete-slash ()
    (let* ((start (max (line-beginning-position) (- (point) 3)))
           (str (buffer-substring-no-properties start (point))))
      (if (cond ((string= str "..") t)
                ((string= str "/..") t)
                ((string= str " ..") t)
                (t nil))
          (list (point) (point) '(("/" 1)))
        nil)
      )))



;;;----------------------------------------
;;; ivy, counsel, swiper
(use-package ivy
  :ensure t
  :diminish ivy-mode
  :custom
  ;; Add recent files to switch buffers list
  (ivy-use-virtual-buffers 'recentf)
  ;; Show index and maximum number of completions
  (ivy-count-format "(%d/%d) ")
  ;; Make the prompt line selectable. Needed when creating a new file
  ;; with a name that would otherwise match an existing completion
  ;; candidate. Example: create file 'foo' when file 'foobar' exists.
  ;; This would select 'foobar' instead of creating 'foo'.
  (ivy-use-selectable-prompt t)
  :config
  (ivy-mode 1)
  ;; Disable mouse buttons in ivy minibuffers
  (define-key ivy-minibuffer-map (kbd "<mouse-1>") nil)
  (define-key ivy-minibuffer-map (kbd "<down-mouse-1>") nil))


(use-package counsel
  :ensure t
  :diminish counsel-mode
  :config
  (counsel-mode 1))


(use-package swiper
  :ensure t
  :bind ("C-c s" . swiper))



;;;----------------------------------------
;;; avy
(use-package avy
  :ensure t
  :bind (("C-c l" . avy-goto-line)
         ("C-c c" . avy-goto-char)))



;;;----------------------------------------
;;; ace-window
(use-package ace-window
  :ensure t
  :bind ("C-x C-o" . ace-window)
  :custom
  ;; Alway show the ace window selection, even if only two buffers are
  ;; visible. Default behaviour would be to switch active buffer if only
  ;; two buffers are available.
  (aw-dispatch-always t))



;;;----------------------------------------
;;; ripgrep
(use-package ripgrep
  :ensure t)



;;;----------------------------------------
;;; all-the-icons
(use-package all-the-icons
  :if (display-graphic-p)
  :ensure t)

;; The rest is loaded only if all required fonts are installed
;; Check if all fonts needed by all-the-icons are installed.
;; Set to nil if fonts are not available or if running in terminal.
(defconst is-all-the-icons-fontset-installed
  (if (display-graphic-p)
      (let ((fonts (mapcar (lambda (x) (aref x 0)) (x-family-fonts))))
        (seq-reduce
         (lambda (x y)
           (and x (when (member y fonts)
                    t)))
         '(all-the-icons file-icons FontAwesome Material\ Icons github-octicons Weather\ Icons)
         t))
    nil))

(when is-all-the-icons-fontset-installed
  ;; Fix Emacs freeze when all-the-icons are enabled (only Windows)
  ;; https://github.com/domtronn/all-the-icons.el/issues/28
  (when (eq system-type 'windows-nt)
    (setq inhibit-compacting-font-caches t))

  (use-package all-the-icons-dired
    :ensure t
    :after all-the-icons
    :diminish all-the-icons-dired-mode
    :hook (dired-mode . all-the-icons-dired-mode))

  ;; This does not work. Why?
;;  (use-package all-the-icons-ivy
;;    :ensure t
;;    :hook (ivy-mode . all-the-icons-ivy-setup))
)



;;;----------------------------------------
;;; Spaceline
(use-package spaceline-config
  :ensure spaceline
  :config
  ;; Build my own spaceline modeline

  (spaceline-define-segment
      mode-icon "An `all-the-icons' segment for the current buffer mode"
      (when is-all-the-icons-fontset-installed
        (let ((icon (all-the-icons-icon-for-buffer)))
          (unless (symbolp icon) ;; This implies it's the major mode
            (propertize icon
                        'help-echo (format "Major-mode: `%s`" major-mode)
                        'display '(raise 0.0)
                        'face `(:height 1.0 :family ,(all-the-icons-icon-family-for-buffer) :inherit))))))

  (spaceline-define-segment projectile-root-with-remote-detection
    "Show the current projectile root if not using TRAMP."
    (if (file-remote-p default-directory)
        "-remote-"
      ;; This part has been copied from spaceline-segments.el
      (when (fboundp 'projectile-project-name)
        (let ((project-name (projectile-project-name)))
          (unless (or (string= project-name "-")
                      (string= project-name (buffer-name)))
            project-name)))))

  (spaceline-install
    'my-theme
    '((buffer-modified
       anzu
       buffer-size)
      ((buffer-id "[" projectile-root-with-remote-detection "]") :face highlight-face)
      ;; mode-icon causes some problems, disabled for now
      (;;mode-icon
       major-mode
       python-env
       (flycheck-error flycheck-warning flycheck-info))
      (minor-modes)
      (global))
    '((version-control)
      ((buffer-encoding-abbrev line-column) :separator " | ")
      (buffer-position)))
  (setq-default mode-line-format '("%e" (:eval (spaceline-ml-my-theme)))))



;;;----------------------------------------
;;; Git
(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status)
  :custom-face
  ;; Set the background colour of the selected line to something visible
  (magit-section-highlight ((t (:inherit secondary-selection :background nil))))
  :config
  ;; Expand stashes
  (add-to-list 'magit-section-initial-visibility-alist '([stashes status] . show))
  ;; Expand recent commits
  (add-to-list 'magit-section-initial-visibility-alist '([unpushed status] . show)))


(use-package magit-svn
  :ensure t
  :after magit  ;; Prevent loading at startup
  ;; Activate magit-svn only when needed
  ;; https://danlamanna.com/svn-externals-with-git-svn-and-magit.html
  :hook (magit-mode . (lambda ()
                        (if (magit-svn-get-ref-info)
                            (magit-svn-mode)))))

(use-package magit-gitflow
  :ensure t
  :diminish magit-gitflow-mode
  :after magit  ;; Prevent loading at startup
  :init
  (setq magit-gitflow-popup-key "C-M-f")
  :hook (magit-mode . (lambda ()
                        (magit-gitflow-mode 1))))

(use-package git-timemachine
  :ensure t)


;;;----------------------------------------
;;; Which Key: show keybindings
(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config
  (which-key-mode 1))



;;;----------------------------------------
;;; Projectile mode
(use-package projectile
  :ensure t
  :diminish projectile-mode
  :bind-keymap
  ;; Enable the 'old' prefix
  ("C-c p" . projectile-command-map)
  :custom
  (projectile-completion-system 'ivy)
  :config
  (projectile-mode 1)
  ;; Enable fast indexing on Windows
  (when (eq system-type 'windows-nt)
    (setq projectile-indexing-method 'alien)))



;;;----------------------------------------
;;; GGTAGS
(use-package ggtags
  :ensure t
  :hook (c-mode-common . (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (ggtags-mode 1))))
  :custom
  ;; ???
  (ggtags-enable-navigation-keys nil)
  :config
  ;; Prevent the GTAGS project root directory from being appended to the
  ;; buffer name in the modeline
  (setq ggtags-mode-line-project-name nil))



;;;----------------------------------------
;;; flyspell
(use-package flyspell
  :ensure t
  :diminish flyspell-mode
  :hook ((markdown-mode . flyspell-mode)
         (prog-mode . flyspell-prog-mode)))



;;;----------------------------------------
;;; flycheck
(use-package flycheck
  :ensure t
  :diminish flycheck-mode
  :config
  ;; Disable flycheck's yaml-ruby checker. jsyaml and yamllint give better results.
  (add-to-list 'flycheck-disabled-checkers 'yaml-ruby)
  ;; Set all python executables to the system wide default. This
  ;; prevents errors when virtual envs are used that do not contain
  ;; the required packages.
  ;; Windows does not have a 'python3' executable, use 'python' instead.
  (let ((python-executable (if (eq system-type 'windows-nt)
                               (executable-find "python")
                             (executable-find "python3"))))
    ;;(setq flycheck-python-flake8-executable python-executable)
    ;;(setq flycheck-python-mypy-executable python-executable)
    ;;(setq flycheck-python-pycompile-executable python-executable)
    (setq flycheck-python-pylint-executable python-executable)
    (setq flycheck-json-python-json-executable python-executable)
    )
  ;; Disable flycheck in C modes because it does not work for my C projects at all.
  (add-hook 'c-mode-common-hook (lambda ()
                                  (flycheck-mode 0)))
  (global-flycheck-mode 1))



;;;----------------------------------------
;;; anzu mode
;;; counts matches of isearch etc. and show them in the modeline
(use-package anzu
  :ensure t
  :demand t
  :bind (("M-%" . anzu-query-replace)  ; Use anzu functions instead of normal query-replace
         ("C-M-%" . anzu-query-replace-regexp))
  :diminish anzu-mode
  :custom
  ;; Prevent anzu mode from changing the modeline. This is handled by
  ;; spaceline instead.
  (anzu-cons-mode-line-p nil)
  :config
  (global-anzu-mode 1))



;;;----------------------------------------
;;; company mode for completion
(use-package company
  :ensure t
  :diminish company-mode
  :config
  (global-company-mode 1))



;;;----------------------------------------
;;; Volatile Highlights mode
;;; Highlight changes like undo, yank etc.
(use-package volatile-highlights
  :ensure t
  :diminish volatile-highlights-mode
  :config
  (volatile-highlights-mode t))



;;;----------------------------------------
;;; Language Server Protocol
(use-package lsp-mode
  :ensure t
  :custom
  (lsp-keymap-prefix "C-c C-l")
  (lsp-auto-guess-root t)
  (lsp-keep-workspace-alive nil)
  (lsp-disabled-clients '(pyls))
  (lsp-pylsp-plugins-pylint-enabled t)
  (lsp-pylsp-plugins-pydocstyle-enabled nil)
  (lsp-pylsp-plugins-flake8-enabled nil)
  (lsp-enable-snippet nil)
  :hook ((python-mode . lsp)
         (clojure-mode . lsp)
         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp)

(use-package lsp-ivy
  :ensure t
  :commands lsp-ivy-workspace-symbol)

(use-package dap-mode
  :ensure t
  :config
  (require 'dap-python)
  :custom
  (dap-python-debugger 'debugpy)
  )



;;;----------------------------------------
;;; Perl
;; Replace default perl-mode with cperl-mode
(defalias 'perl-mode 'cperl-mode)



;;;----------------------------------------
;;; CMake
(use-package cmake-mode
  :ensure t)

;;; Bazel
(use-package bazel
  :ensure t
  :config
  ;; Use starlark mode for copybara config files
  (add-to-list 'auto-mode-alist '("\\.sky\\'" . bazel-starlark-mode)))



;;;----------------------------------------
;;; Python virtual environments
(use-package virtualenvwrapper
  :ensure t
  :custom
  (ven-location (getenv "WORKON_HOME"))
  :config
  (venv-initialize-interactive-shells)
  (venv-initialize-eshell))



;;;----------------------------------------
;;; Markdown mode
(use-package markdown-mode
  :ensure t)



;;;----------------------------------------
;;; JSON mode
(use-package json-mode
  :ensure t)



;;;----------------------------------------
;;; YAML mode
(use-package yaml-mode
  :ensure t)



;;;----------------------------------------
;;; Groovy mode
(use-package groovy-mode
  :ensure t)

(use-package jenkinsfile-mode
  :ensure t)



;;;----------------------------------------
;;; PlantUML
(use-package plantuml-mode
  :ensure t
  :custom
  (plantuml-default-exec-mode 'jar)
  :config
  (plantuml-set-output-type "png"))



;; ;;;----------------------------------------
;; ;;; Lisp mode
;;
;; ;;; SLIME
;; ;; Setup load-path and autoloads
;; ;;(add-to-list 'load-path "~/dir/to/cloned/slime")
;; (require 'slime)
;; (require 'slime-autoloads)
;; (require 'slime-company)
;;
;; ;; Set your lisp system and some contribs
;; (setq inferior-lisp-program "sbcl")
;; (setq slime-lisp-implementations
;;       '((sbcl ("sbcl") :coding-system utf-8-unix)))
;; (setq slime-company-completion 'fuzzy)
;; (setq slime-company-complete-in-comments-and-strings t)
;; ;;(setq slime-contribs '(slime-fancy slime-company))
;; (slime-setup '(slime-fancy slime-company slime-indentation))



;;;----------------------------------------
;;; Clojure mode
(use-package clojure-mode
  :ensure t)

(use-package cider
  :ensure t
  :hook (clojure-mode . cider-mode))



;;;----------------------------------------
;;; Rust
(use-package rustic
  :ensure t
  )



;;;----------------------------------------
;; Docker
(use-package docker
  :ensure t)

(use-package dockerfile-mode
  :ensure t)

(use-package docker-compose-mode
  :ensure t)

(when (< emacs-major-version 29)
  (use-package docker-tramp
    :ensure t))



;;;----------------------------------------
;;; wgrep, Writable grep window
(use-package wgrep
  :ensure t)



;;;----------------------------------------
;;; Undo tree mode
(use-package undo-tree
  :ensure t
  :diminish undo-tree-mode
  :custom
  ;; Do not save undo history to file
  (undo-tree-auto-save-history nil)
  :config
  (global-undo-tree-mode))



;;;----------------------------------------
;; vlf
(use-package vlf
  :ensure t)



;;;----------------------------------------
;; Parentheses and pairs
(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :hook (clojure-mode . smartparens-strict-mode)
  :init
  (require 'smartparens-config)
  ;; Disable highlighting of the region between inserted matching pairs
  (setq sp-highlight-pair-overlay nil)
  ;; Highlight matching pair, same as show-paren-mode
  (show-smartparens-global-mode 1)
  (smartparens-global-mode 1)
  (global-set-key (kbd "C-c )") 'sp-forward-slurp-sexp)
  (global-set-key (kbd "C-c }") 'sp-forward-barf-sexp)
  (global-set-key (kbd "C-c (") 'sp-backward-slurp-sexp)
  (global-set-key (kbd "C-c {") 'sp-backward-barf-sexp))

;; Set a different face for parentheses in all prog modes
(use-package paren-face
  :ensure t
  :hook (prog-mode . paren-face-mode))



;;;----------------------------------------
;;; My custom functions
(defun my-kill-unmod-buffers ()
  "Kill all unmodified buffers."
  (interactive)
  (mapcar 'my-kill-buffers-if-unmod
           (buffer-list)))

(defun my-kill-other-unmod-buffers ()
  "Kill all unmodified buffers except the currently visited one."
  (interactive)
  (mapcar (lambda (buf)
            (unless (eq (current-buffer) buf)
              (my-kill-buffers-if-unmod buf)))
          (buffer-list)))

(defun my-kill-buffers-if-unmod (buf)
  "Kill buffer BUF if it has not been modified."
  (unless (buffer-modified-p buf)
    (kill-buffer buf)))

(global-set-key (kbd "C-c m k") 'my-kill-unmod-buffers)
(global-set-key (kbd "C-c m C-k") 'my-kill-other-unmod-buffers)



;;;----------------------------------------
;;; Remove all unwanted minor modes from the modeline
(use-package diminish
  :ensure t)



(diminish 'eldoc-mode)


;;; init.el ends here
